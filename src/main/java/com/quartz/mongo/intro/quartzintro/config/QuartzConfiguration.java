package com.quartz.mongo.intro.quartzintro.config;

import com.quartz.mongo.intro.quartzintro.service.JobsListenerService;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import java.io.IOException;
import java.util.Properties;

/**
 * This class will configure and setup quartz using the
 * {@link SchedulerFactoryBean}
 * 
 * @author dinuka
 *
 */
@Configuration
@EnableAsync
@PropertySource("classpath:quartz.properties")
public class QuartzConfiguration {

	/**
	 * Here we integrate quartz with Spring and let Spring manage initializing
	 * quartz as a spring bean.
	 * 
	 * @return an instance of {@link SchedulerFactoryBean} which will be managed
	 *         by spring.
	 */
	@Bean
	public SchedulerFactoryBean schedulerFactoryBean(
			JobFactory jobFactory,
			ConfigurableEnvironment env,
			JobsListenerService jobsListenerService
	) throws IOException {
		final Properties props = resolveProperties(env, quartzProperties());
		SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
		scheduler.setWaitForJobsToCompleteOnShutdown(true);
		scheduler.setJobFactory(jobFactory);
		scheduler.setQuartzProperties(props);
		scheduler.setGlobalJobListeners(jobsListenerService);
		return scheduler;
	}

	private Properties resolveProperties(ConfigurableEnvironment env, Properties source) {
		final Properties newProps = new Properties();
		source.forEach((key1, value) -> {
			final String key = key1.toString();
			final String newValue = env.resolvePlaceholders(value.toString());
			newProps.setProperty(key, newValue);
		});
		return newProps;
	}

	@Bean
	public JobFactory jobFactory(
			ApplicationContext applicationContext
	) {
		AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
		jobFactory.setApplicationContext(applicationContext);
		return jobFactory;
	}

	@Bean
	public Properties quartzProperties() throws IOException {
		PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
		propertiesFactoryBean.setLocation(new ClassPathResource("/quartz.properties"));
		propertiesFactoryBean.afterPropertiesSet();
		return propertiesFactoryBean.getObject();
	}

}
