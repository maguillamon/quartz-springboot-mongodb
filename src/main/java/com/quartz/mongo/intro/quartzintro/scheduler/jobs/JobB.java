package com.quartz.mongo.intro.quartzintro.scheduler.jobs;

import org.apache.commons.lang3.RandomUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

@PersistJobDataAfterExecution
@DisallowConcurrentExecution()
public class JobB extends QuartzJobBean {

    private static Logger log = LoggerFactory.getLogger(JobB.class);



    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        final String jobName = context.getJobDetail().getKey().getName();
        log.info("[Job-B][{}] Starting sleeping", jobName);
        final long sleepMs = RandomUtils.nextLong(500L, 6000L);
        try {
            Thread.sleep(sleepMs);
        } catch (InterruptedException e) {
            log.error("[Job-B][{}] Error in sleeping", jobName, e);
        }
        log.info("[Job-B][{}] Ending sleeps", jobName);
    }
}
