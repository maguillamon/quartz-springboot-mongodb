package com.quartz.mongo.intro.quartzintro.service;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Service for monitoring the jobs execution (even jobs which execution failed or was vetoed)
 */
@Service
public class JobsListenerService implements JobListener {

    Logger log = LoggerFactory.getLogger(JobsListenerService.class);

    @Override
    public String getName() {
        return "My Jobs Listener";
    }

    @Override
    public void jobToBeExecuted(JobExecutionContext jobExecutionContext) {
        log.info("Ready to execute job: " + jobExecutionContext.getJobDetail().getKey().getName());
    }

    @Override
    public void jobExecutionVetoed(JobExecutionContext jobExecutionContext) {
        log.info("Job " + jobExecutionContext.getJobDetail().getKey().getName() + " was vetoed");
    }

    @Override
    public void jobWasExecuted(JobExecutionContext jobExecutionContext, JobExecutionException e) {
        if (e!= null) {
            log.error("Job: " + jobExecutionContext.getJobDetail().getKey().getName() + " was executed, but threw exception: " + e.toString());
        }
        log.info("Job " + jobExecutionContext.getJobDetail().getKey().getName() + " was executed taking " + jobExecutionContext.getJobRunTime() + " ms to be completed");
    }
}
