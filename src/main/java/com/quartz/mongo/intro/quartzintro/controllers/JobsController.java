package com.quartz.mongo.intro.quartzintro.controllers;

import com.quartz.mongo.intro.quartzintro.scheduler.jobs.JobA;
import com.quartz.mongo.intro.quartzintro.scheduler.jobs.JobB;
import com.quartz.mongo.intro.quartzintro.service.JobsService;
import org.apache.commons.lang3.RandomUtils;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;

@RestController
@RequestMapping("/my-jobs")
public class JobsController {

    public static final String GROUP_A = "group_a";
    public static final String GROUP_B = "group_b";


    private final Logger log = LoggerFactory.getLogger(getClass());

    private final JobsService jobsService;

    public JobsController(JobsService jobsService) {
        this.jobsService = jobsService;
    }


    @GetMapping("/a")
    public String createJobA() {
        final int jobs = RandomUtils.nextInt(3, 6);
        final List<String> jobsIds = new ArrayList<>();

        IntStream.range(1, jobs).forEach(i -> {
            final String jobId = i + "_JobA__" + UUID.randomUUID().toString();
            //final String jobId = "jobA";
            final JobDetail jobDetail = JobBuilder.newJob(JobA.class)
                    .withIdentity(jobId, GROUP_A)
                    .requestRecovery(true)
                    .build();
            final Trigger triggerA = TriggerBuilder.newTrigger()
                    .withIdentity(jobId + "-trigger", GROUP_A)
                    .startAt(DateBuilder.futureDate(10, DateBuilder.IntervalUnit.SECOND))
                    .build();

            jobsIds.add(jobId);
            try {
                jobsService.queueJob(jobDetail, triggerA);
            }
            catch (Exception ex) {
                log.error("Error in job A", ex);
            }
        });

        return String.join(",", jobsIds);
    }

    @GetMapping("/b")
    public String createJobB() {
        final int jobs = RandomUtils.nextInt(3, 6);
        final List<String> jobsIds = new ArrayList<>();

        IntStream.range(1, jobs).forEach(i -> {
            final String jobId = i + "_JobB__" + UUID.randomUUID().toString();
            //final String jobId = "jobB";
            final JobDetail jobDetail = JobBuilder.newJob(JobB.class)
                    .withIdentity(jobId, GROUP_B)
                    .requestRecovery(true)
                    .build();
            final Trigger triggerB = TriggerBuilder.newTrigger()
                    .withIdentity(jobId + "-trigger", GROUP_B)
                    .startAt(DateBuilder.futureDate(20, DateBuilder.IntervalUnit.SECOND))
                    .build();
            jobsIds.add(jobId);
            try {
                jobsService.queueJob(jobDetail, triggerB);
            }
            catch (Exception ex) {
                log.error("Error in job B", ex);
            }
        });

        return String.join(",", jobsIds);
    }

    @GetMapping("/both")
    public String bothsJobs() {
        final StringBuilder sb = new StringBuilder();
        final int timesA = RandomUtils.nextInt(2, 5);
        final int timesB = RandomUtils.nextInt(2, 5);

        IntStream.range(1, timesA).forEach(i -> {
            final String result = createJobA();
            sb.append(result).append(",");
        });
        IntStream.range(1, timesB).forEach(value -> {
            final String result = createJobB();
            sb.append(result).append(",");
        });

        return sb.toString();
    }

}
