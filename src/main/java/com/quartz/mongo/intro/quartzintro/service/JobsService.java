package com.quartz.mongo.intro.quartzintro.service;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.utils.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class JobsService {

    private final Scheduler scheduler;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    public JobsService(SchedulerFactoryBean schedulerFactory) {
        this.scheduler = schedulerFactory.getScheduler();
    }

    public void queueJob(
            JobDetail job, Trigger trigger
    ) throws SchedulerException {
        scheduler.scheduleJob(job, trigger);
    }

    public List<String> getJobs(String groupName) throws SchedulerException {
        return scheduler
                .getJobKeys(GroupMatcher.jobGroupEquals(groupName))
                .stream()
                .map(Key::getName)
                .sorted(Comparator.naturalOrder())
                .collect(Collectors.toList());
    }

}
