package com.quartz.mongo.intro.quartzintro.scheduler.jobs;

import com.quartz.mongo.intro.quartzintro.controllers.JobsController;
import com.quartz.mongo.intro.quartzintro.service.JobsService;
import org.apache.commons.lang3.RandomUtils;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 *
 * This is the job class that will be triggered based on the job configuration
 *
 *
 * @author dinuka
 *
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Component
public class JobA extends QuartzJobBean {

	private static Logger log = LoggerFactory.getLogger(JobA.class);

	@Autowired
	private JobsService jobsService;



	/**
	 * This is the method that will be executed each time the trigger is fired.
	 */
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		final String jobName = context.getJobDetail().getKey().getName();
		log.info("[Job-A][{}] Starting sleeping", jobName);
		final long sleepMs = RandomUtils.nextLong(500L, 6000L);
		try {
			Thread.sleep(sleepMs);
		} catch (InterruptedException e) {
			log.error("[Job-A][{}] Error in sleeping", jobName, e);
		}

		List<String> jobNames = Collections.emptyList();
		try {
			jobNames = jobsService.getJobs(JobsController.GROUP_A);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
		log.info("[Job-A][{}] All jobs {}", jobName, jobNames);
		log.info("[Job-A][{}] Ending sleeps", jobName);
	}
}
