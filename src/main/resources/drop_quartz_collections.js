/*
    This scripts drops Quartz related collections from MongoDB in order to do fresh testings
 */
//Warning: This command obtains a write lock on the affected database
//and will block other operations until it has completed.
db.quartz__calendars.drop();
db.quartz__jobs.drop();
db.quartz__locks.drop();
db.quartz__schedulers.drop();
db.quartz__triggers.drop();